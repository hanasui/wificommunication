﻿namespace WifiCommunication
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;

    public class Program
    {
        [DllImport("KERNEL32.DLL")]
        public static extern uint
          GetPrivateProfileString(string lpAppName,
          string lpKeyName, string lpDefault,
          StringBuilder lpReturnedString, uint nSize,
          string lpFileName);

        static String CONFIG_FILE = "../../../config.ini";
        static int DEVICE_NUM = 3;
        public static void Main()
        {
            StringBuilder port = new StringBuilder(8);
            StringBuilder host = new StringBuilder(32);
            System.Net.Sockets.TcpClient[] tcp = new System.Net.Sockets.TcpClient[DEVICE_NUM];
            System.Net.Sockets.NetworkStream[] ns = new System.Net.Sockets.NetworkStream[DEVICE_NUM];
            
            for(int i=0; i<DEVICE_NUM; i++)
            {
                GetPrivateProfileString("Config", "XBEE"+i.ToString()+"_IP", "default", host, Convert.ToUInt32(host.Capacity), CONFIG_FILE);
                GetPrivateProfileString("Config", "XBEE"+i.ToString()+"_PORT", "default", port, Convert.ToUInt32(port.Capacity), CONFIG_FILE);
                
                tcp[i] =
                    new System.Net.Sockets.TcpClient(host.ToString(), int.Parse(port.ToString()));
                Console.WriteLine("サーバー({0}:{1})と接続しました({2}:{3})。",
                    ((System.Net.IPEndPoint)tcp[i].Client.RemoteEndPoint).Address,
                    ((System.Net.IPEndPoint)tcp[i].Client.RemoteEndPoint).Port,
                    ((System.Net.IPEndPoint)tcp[i].Client.LocalEndPoint).Address,
                    ((System.Net.IPEndPoint)tcp[i].Client.LocalEndPoint).Port);
                ns[i] = tcp[i].GetStream();
            }

            while (true)
            {
                Console.WriteLine("サウンドを変更したいデバイスの番号（0～3）を入力してください");
                int d_num = int.Parse(Console.ReadLine());

                Console.WriteLine("サウンド名を入力してください");
                string sendMsg = Console.ReadLine();

                if (sendMsg == null || sendMsg.Length == 0)
                {
                    break;
                }
                System.Text.Encoding enc = System.Text.Encoding.UTF8;
                byte[] sendBytes = enc.GetBytes(sendMsg);

                //データを送信する
                ns[d_num].Write(sendBytes, 0, sendBytes.Length);
                Console.WriteLine(sendMsg);
            }
            for (int i = 0; i < DEVICE_NUM; i++)
            {
                ns[i].Close();
                tcp[i].Close();
            }
            Console.WriteLine("切断しました。");

            Console.ReadLine();
        }
    }
}
